﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            const double mile = 1.609344;
            const double kilometre = 0.621371;
            var answer = 0.00;
            var total = 0.00;

            Console.WriteLine("Please enter kilometres so that we can convert into miles");
            answer = double.Parse(Console.ReadLine());
            total = answer * mile;
            Console.WriteLine($"Your answer is {total} miles");
            

        }
    }
}
